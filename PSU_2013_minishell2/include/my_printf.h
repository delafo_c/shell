/*
** my_printf.h for my_printf in /home/delafo_c/travail/PrintF
** 
** Made by delafo_c
** Login   <delafo_c@epitech.net>
** 
** Started on  Fri Nov 15 17:19:51 2013 delafo_c
** Last update Thu Dec 19 12:21:28 2013 delafo_c
*/

#ifndef MY_PRINTF_
# define MY_PRINTF_

#include <stdarg.h>

/*
** MY_PRINTF
*/

typedef int	(*handler)(va_list ap);

int		my_printf(const char *format, ...);
int		my_printf_norme(const char *format, va_list ap,
				char *printf_tab, handler *printf_flag);
int		check_flag(char format, char *str);

/*
** FLAGS FOR MY_PRINTF
*/

int		printf_dec(va_list ap);
int		printf_octal(va_list ap);
int		printf_hex(va_list ap);
int		printf_hexmaj(va_list ap);
int		printf_u(va_list ap);
int		printf_bin(va_list ap);
int		printf_c(va_list ap);
int		printf_s(va_list ap);
int		printf_smaj(va_list ap);
int		printf_p(va_list ap);
int		printf_space(va_list ap);
int		printf_percent(va_list ap);

/*
** MY_PUTNBR FOR MY_PRINTF
*/

int		my_put_nbr_printf(int nb);
int		my_putnbr_base_printf(unsigned int nb, char *base, int result);

#endif /* MY_PRINTF_ */
