/*
** minishell1.h for minishell1 in /home/delafo_c/rendu/PSU_2013_minishell1
** 
** Made by delafo_c
** Login   <delafo_c@epitech.net>
** 
** Started on  Tue Dec 10 00:50:59 2013 delafo_c
** Last update Sun Mar  9 21:45:55 2014 delafo_c
*/

#ifndef MINISHELL2_H_
# define MINISHELL2_H_

# define CMD		0
# define BUILTINS	1
# define SIZEBUF	1024

# define SEP		";"
# define PIPE		"|"
# define REDI_R		">"
# define D_REDI_R	">>"
# define REDI_L		"<"
# define D_REDI_L	"<<"

# define TYPE_SEP	1
# define TYPE_PIPE	2
# define TYPE_REDI_R	3
# define TYPE_D_REDI_R	4
# define TYPE_REDI_L	5
# define TYPE_D_REDI_L	6

/*
** ENV LINKED LIST
*/

typedef struct	s_env
{
  char		*var;
  struct s_env	*next;
  struct s_env	*prev;
}		t_env;

/*
** CMD LINKED LIST
*/

typedef struct	s_cmd
{
  int		type;
  char		*word;
  struct s_cmd	*next;
  struct s_cmd	*prev;
}		t_cmd;

/*
** PRINCIPAL STRUCTURE
*/

typedef struct	s_data
{
  int		tok;
  t_env		*env;
  char		*path;
  char		*home;
  char		*pwd;
  char		*old_pwd;
}		t_data;

/*
** BUILTINS
*/

typedef struct	s_builtins
{
  char		*name;
  int		(*ptr_func)(t_data *data, char **cmd, char **env_tab, int count, int i);
}		t_builtins;

extern t_builtins	builtins[];

/*
** PARSER
*/

typedef struct	s_token
{
  char		*name;
  int		type;
}		t_token;

char		**my_str_to_wordtab(char *str);
int		my_countword(char *str);
int		my_countchar(char *str);
int		free_tab(char **tab);
t_cmd		*my_str_to_dll(char *str, t_cmd *cmd);
int		gere_others(char *str, t_cmd *cmd, int i);
void		gere_spaces(char *cmd, char *good_cmd);
int		gere_spaces_and_tabs(char *str, int i);
int		get_nb_sep(char *cmd);
char		*add_spaces(char *cmd);

/*
** EXECUTION
*/

void		prompt(void);
t_data		*init_data(char **environ);
int		gere_exec(t_cmd *command, t_data *data, char **environ);
int		my_exec(char **cmd, char **env, t_data *data);
int		exec_tok(t_data *data, char **cmd, char **env_tab);
int		check_tok(char **cmd, t_data *data);
void		free_data(t_data *data);

/*
** PATH
*/

char		*find_path(char *cmd, char **env);
char		**my_strtok_path(char *env_path);
char		*get_path(char **env);
char		*try_path(char *cmd, char **path);
int		count_path(char *path);
int		my_pathlen(char *path);

/*
** ENVIRONNEMENT
*/

t_env		*init_env(char **environ);
t_env		*init_dll_env();
t_env		*put_in_list(t_env *env, char *var);
char		**env_in_tab(t_env *env);

/*
** COMMAND
*/

t_cmd		*init_cmd(char *command);
t_cmd		*init_dll_cmd(void);
t_cmd		*add_to_list(t_cmd *cmd, char *word);
char		**cmd_in_tab(t_cmd *cmd);
void		free_command(char *cmd, t_cmd *command);

/*
** BUILTINS
*/

int		my_cd(t_data *data, char **cmd, char **env_tab, int count, int i);
int		cd_dash(t_data *data);
int		cd_path(char **cmd, t_data *data);
int		cd_home(t_data *data);
char		*get_home(char **env);
char		*get_pwd(char **env);
int		new_pwd(t_data *data, char *path);
int		my_env(t_data *data, char **cmd, char **env_tab, int count, int i);
t_env		*find_var(t_env *env, char *var);
int		add_to_env(char **cmd, t_env *env);
int		my_setenv(t_data *data, char **cmd, char **env_tab, int count, int i);
int		remove_var(char *var_name, t_env *env);
int		my_unsetenv(t_data *data, char **cmd, char **env_tab, int count, int i);

#endif /* MINISHELL2_H_ */
