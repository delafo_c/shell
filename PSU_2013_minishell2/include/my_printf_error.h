/*
** my_printf.h for my_printf in /home/delafo_c/travail/PrintF
** 
** Made by delafo_c
** Login   <delafo_c@epitech.net>
** 
** Started on  Fri Nov 15 17:19:51 2013 delafo_c
** Last update Mon Jan  6 21:54:37 2014 delafo_c
*/

#ifndef _MY_PRINTF_ERROR_
#define _MY_PRINTF_ERROR_

#include <stdarg.h>

/*
** MY_PRINTF_ERROR
*/

typedef int	(*handler)(va_list ap);

int		my_printf_error(const char *format, ...);
int		my_printf_norme_error(const char *format, va_list ap,
				      char *printf_tab, handler *printf_flag);
int		check_flag_error(char format, char *str);

/*
** FLAGS_ERROR FOR MY_PRINTF_ERROR
*/

int		printf_dec_error(va_list ap);
int		printf_octal_error(va_list ap);
int		printf_hex_error(va_list ap);
int		printf_hexmaj_error(va_list ap);
int		printf_u_error(va_list ap);
int		printf_bin_error(va_list ap);
int		printf_c_error(va_list ap);
int		printf_s_error(va_list ap);
int		printf_smaj_error(va_list ap);
int		printf_p_error(va_list ap);
int		printf_space_error(va_list ap);
int		printf_percent_error(va_list ap);

/*
** MY_PUTNBR_ERROR FOR MY_PRINTF_ERROR
*/

int		my_put_nbr_printf_error(int nb);
int		my_putnbr_base_printf_error(unsigned int nb, char *base, int result);

#endif /* _MY_PRINTF_ERROR_ */
