/*
** my.h for my in /home/delafo_c/rendu/Piscine-C-include
** 
** Made by delafo_c
** Login   <delafo_c@epitech.net>
** 
** Started on  Thu Oct 10 12:06:30 2013 delafo_c
** Last update Tue Jan  7 15:18:29 2014 delafo_c
*/

#ifndef	MY_H_
#define	MY_H_

void	my_putchar(char c);
int	my_isneg(int nb);
int	my_put_nbr(int nb);
int	my_putnbr_base(int nb, char *base);
void	my_swap(int *a, int *b);
void	my_swap_char(char **a, char **b);
void	my_putstr(char *str);
int	my_strlen(char *str);
int	my_getnbr(char *str);
int	my_getnbr_base(char *str, char *base);
int	my_power_rec(int nb, int power);
char	*my_strcpy(char *dest, char *src);
char	*my_strncpy(char *dest, char *src, int nb);
char	*my_revstr(char *str);
char	*my_strstr(char *str, char *to_find);
int	my_strcmp(char *s1, char *s2);
int	my_strncmp(char *s1, char *s2, int nb);
char	*my_strcat(char *dest, char *src);
char	*my_strncat(char *dest, char *src, int nb);
int	my_strlcat(char *dest, char *src, int size);
int	is_op(char c);
int	is_num(char c);
int	is_alpha(char c);
char	*my_tolower(char *str);

#endif /* MY_H_ */
