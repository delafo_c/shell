/*
** get_next_line.h for get_next_line in /home/delafo_c/travail/GetNextLine
** 
** Made by delafo_c
** Login   <delafo_c@epitech.net>
** 
** Started on  Mon Nov 18 21:04:39 2013 delafo_c
** Last update Sun Mar  9 22:23:37 2014 delafo_c
*/

#ifndef GET_NEXT_LINE_H_
# define GET_NEXT_LINE_H_
# define NBCHAR		1024
# define BUFSIZE	NBCHAR + 1

char		*get_next_line(const int fd);
char		*minish(void);
int		relocbuf(char *buf, int cur);
void		empty(char *next_line);

#endif /* GET_NEXT_LINE_H_ */
