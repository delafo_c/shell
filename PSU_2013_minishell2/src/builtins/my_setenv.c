/*
** my_setenv.c for my_setenv in /home/delafo_c/rendu/PSU_2013_minishell1/builtins
** 
** Made by delafo_c
** Login   <delafo_c@epitech.net>
** 
** Started on  Thu Dec 12 00:34:08 2013 delafo_c
** Last update Sun Mar  9 23:26:29 2014 delafo_c
*/

#include <stdlib.h>
#include "../../include/my.h"
#include "../../include/my_printf.h"
#include "../../include/minishell2.h"

t_env		*find_var(t_env *env, char *var)
{
  t_env		*tmp;

  tmp = env;
  env = env->next;
  while (tmp != env)
    {
      if (my_strncmp(var, env->var, my_strlen(var)) == 1)
	return (env);
      env = env->next;
    }
  return (tmp);
}

int		add_to_env(char **cmd, t_env *env)
{
  t_env		*var_env;
  char		*var_name;
  char		*var_full;

  if ((var_name = my_strcat(cmd[1], "=")) == NULL
      || (var_env = find_var(env, var_name)) == NULL)
    return (-1);
  if (cmd[2] == NULL)
    var_full = my_strdup(var_name);
  else
    if ((var_full = my_strcat(var_name, cmd[2])) == NULL)
      return (-1);
  free(var_name);
  if (var_env == env)
    put_in_list(env, var_full);
  else
    {
      free(var_env->var);
      var_env->var = var_full;
    }
  return (0);
}

int		my_setenv(t_data *data, char **cmd, char **env_tab, int count, int i)
{
  if (count > 3)
    {
      my_printf_error("setenv: error: too many arguments\n", 1);
      return (0);
    }
  if (count == 1)
    return (my_env(data, cmd, env_tab, count, i));
  return (add_to_env(cmd, data->env));
}
