/*
** my_unsetenv.c for my_unsetenv.c in /home/delafo_c/rendu/PSU_2013_minishell1/builtins
** 
** Made by delafo_c
** Login   <delafo_c@epitech.net>
** 
** Started on  Thu Dec 12 00:39:33 2013 delafo_c
** Last update Tue Mar  4 16:20:19 2014 delafo_c
*/

#include <stdlib.h>
#include "../../include/my.h"
#include "../../include/my_printf.h"
#include "../../include/minishell2.h"

int		remove_var(char *var_name, t_env *env)
{
  t_env		*tmp;
  char		*tmp_var;

  tmp_var = var_name;
  tmp = env->next;
  if ((var_name = my_strcat(var_name, "=")) == NULL)
    return (-1);
  while (tmp != env)
    {
      if (my_strncmp(var_name, tmp->var, my_strlen(var_name)) == 1)
	{
	  tmp->prev->next = tmp->next;
	  tmp->next->prev = tmp->prev;
	  free(tmp->var);
	  free(tmp);
	  free(var_name);
	  return (0);
	}
      tmp = tmp->next;
    }
  my_printf_error("unsetenv: error : %s : is not a var\n", tmp_var);
  return (-1);
}

int		my_unsetenv(t_data *data, char **cmd, char **env_tab, int count, int i)
{
  i = 1;
  if (count < 2)
    {
      my_printf_error("unsetenv: too few arguments\n");
      return (-1);
    }
  while (i < count)
    {
      if (remove_var(cmd[i], data->env) == -1)
	return (-1);
      i++;
    }
  return (0);
}
