/*
** my_env.c for my_env in /home/delafo_c/rendu/PSU_2013_minishell1/builtins
** 
** Made by delafo_c
** Login   <delafo_c@epitech.net>
** 
** Started on  Thu Dec 12 00:38:16 2013 delafo_c
** Last update Tue Feb 25 13:19:57 2014 delafo_c
*/

#include <stdlib.h>
#include "../../include/my.h"
#include "../../include/my_printf.h"
#include "../../include/minishell2.h"

int		my_env(t_data *data, char **cmd, char **env_tab, int count, int i)
{
  t_env		*tmp;

  tmp = data->env->next;
  while (tmp != data->env)
    {
      my_printf("%s\n", tmp->var);
      tmp = tmp->next;
    }
  return (0);
}
