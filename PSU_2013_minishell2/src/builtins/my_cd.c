/*
** my_cd.c for my_cd in /home/delafo_c/rendu/PSU_2013_minishell1/builtins
** 
** Made by delafo_c
** Login   <delafo_c@epitech.net>
** 
** Started on  Thu Dec 12 00:44:49 2013 delafo_c
** Last update Sun Mar  9 23:33:43 2014 delafo_c
*/

#include <unistd.h>
#include <stdlib.h>
#include "../../include/my.h"
#include "../../include/minishell2.h"

int		new_pwd(t_data *data, char *path)
{
  t_env		*tmp;

  tmp = data->env->next;
  while (tmp != data->env)
    {
      if (my_strncmp(tmp->var, "PWD=", 4) != 0)
	{
	  if ((tmp->var = my_strcat("PWD=", path)) == NULL)
	    return (-1);
	  return (0);
	}
      tmp = tmp->next;
    }
  return (0);
}

int		cd_home(t_data *data)
{
  if (data->home == NULL)
    return (0);
  if (chdir(data->home) == -1)
    my_printf_error("minish: error: could not access home directory\n");
  else
    {
      data->old_pwd = data->pwd;
      data->pwd = data->home;
      return (new_pwd(data, data->home));
    }
  return (0);
}

int		cd_path(char **cmd, t_data *data)
{
  char		*path;

  if ((path = my_strdup(cmd[1])) == NULL)
    return (0);
  if (chdir(path) == -1)
    my_printf_error("cd: error: could not access directory\n");
  else
    {
      if (path[0] != '/')
	{
	  if ((path = my_strcat("/", path)) == NULL
	      || (path = my_strcat(data->pwd, path)) == NULL)
	    return (-1);
	}
      data->old_pwd = data->pwd;
      data->pwd = path;
      return (new_pwd(data, path));
    }
  return (0);
}

int		my_cd(t_data *data, char **cmd, char **env_tab, int count, int i)
{
  data->home = get_home(env_tab);
  data->pwd = get_pwd(env_tab);
  data->old_pwd = get_pwd(env_tab);
  if (count == 1)
    return (cd_home(data));
  if (count > 2)
    my_printf_error("cd: error: too many arguments\n");
  else
    return (cd_path(cmd, data));
  return (0);
}
