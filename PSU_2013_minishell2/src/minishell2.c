/*
** minishell1.c for minishell1 in /home/delafo_c/rendu/PSU_2013_minishell1
** 
** Made by delafo_c
** Login   <delafo_c@epitech.net>
** 
** Started on  Tue Dec 10 00:39:09 2013 delafo_c
** Last update Sun Mar  9 22:53:01 2014 delafo_c
*/

#include <stdlib.h>
#include <signal.h>
#include "../include/my.h"
#include "../include/my_printf.h"
#include "../include/minishell2.h"

/*
** Réaffiche le prompt si le signal SIGINT (Ctrl-C) est catché
*/

void		gere_signal(int sig)
{
  if (signal(sig, gere_signal) == SIG_ERR)
    my_printf_error("minish: error: signal failed\n");
  my_printf("\n");
  prompt();
}

/*
** Affiche le prompt
** Récupère la commande écrite sur l'entrée standard par l'utilisateur
*/

char		*get_cmd(t_data *data)
{
  char		*cmd;

  prompt();
  if ((cmd = get_next_line(0)) == NULL)
    return (NULL);
  if (my_strcmp(cmd, " ") || my_strcmp(cmd, "\t"))
    get_cmd(data);
  return (cmd);
}

/*
** Boucle d'execution
** 1. Récupère la commande
** 2. Ajoute des espaces si besoin
** 3. Création de la liste chainée de commande
** 4. Envoi des commandes, de l'environnement et des données à l'execution
*/

int		minishell2(t_data *data, char **environ)
{
  char		*cmd;
  t_cmd		*command;

  while ((cmd = get_cmd(data)) != NULL)
    {
      if ((command = init_cmd(cmd)) == NULL)
	return (-1);
      gere_exec(command, data, environ);
      free_command(cmd, command);
    }
  return (0);
}

/*
** Affiche le prompt
*/

void		prompt()
{
  my_printf("\033[32m$> \033[0m");
}

/*
** Fonction principale
** 1. Initialisation des données
** 2. Appel de la boucle d'execution
** 3. Libération des données
*/

int		main(int ac, char **av, char **environ)
{
  t_data	*data;

  signal(SIGINT, gere_signal);
  my_printf("\033[32mminish\n\033[0m");
  if ((data = init_data(environ)) == NULL)
    return (-1);
  minishell2(data, environ);
  free_data(data);
  return (0);
}
