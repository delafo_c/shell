/*
** spaces.c for spaces in /home/delafo_c/rendu/PSU_2013_minishell2/src
** 
** Made by delafo_c
** Login   <delafo_c@epitech.net>
** 
** Started on  Mon Feb 24 09:50:07 2014 delafo_c
** Last update Sun Mar  9 22:06:13 2014 delafo_c
*/

#include <stdlib.h>
#include "../include/my.h"
#include "../include/minishell2.h"

const t_token	token_tab[] =
  {
    {SEP, TYPE_SEP},
    {PIPE, TYPE_PIPE},
    {REDI_R, TYPE_REDI_R},
    {D_REDI_R, TYPE_D_REDI_R},
    {REDI_L, TYPE_REDI_L},
    {D_REDI_L, TYPE_D_REDI_L}
  };

char		*add_spaces(char *cmd)
{
  char		*good_cmd;

  if ((good_cmd = malloc(my_strlen(cmd) + 2 * get_nb_sep(cmd) + 1)) == NULL)
    return (NULL);
  gere_spaces(cmd, good_cmd);
  free(cmd);
  return (good_cmd);
}

int		get_nb_sep(char *cmd)
{
  int		i;
  int		j;
  int		nb_sep;

  i = 0;
  nb_sep = 0;
  while (cmd[i])
    {
      j = 0;
      while (token_tab[j].name && !my_strncmp(cmd + i, token_tab[j].name,
					     my_strlen(token_tab[j].name)))
	j++;
      if (token_tab[j].name)
	{
	  i += my_strlen(token_tab[j].name);
	  nb_sep++;
	}
      else
	i++;
    }
  return (nb_sep);
}

void		gere_spaces(char *cmd, char *good_cmd)
{
  int		i;
  int		j;
  int		k;

  i = 0;
  j = 0;
  while (cmd[i])
    {
      k = 0;
      good_cmd[j++] = ' ';
      while (token_tab[k].name && !my_strncmp(cmd + i, token_tab[k].name,
					     my_strlen(token_tab[k].name)))
	k++;
      if (token_tab[k].name && my_strlen(token_tab[k].name) > 1)
	good_cmd[j++] = cmd[i++];
      else if (!token_tab[k].name)
	j--;
      good_cmd[j++] = cmd[i++];
      if (token_tab[k].name)
	good_cmd[j++] = ' ';
    }
  good_cmd[j] = '\0';
}
