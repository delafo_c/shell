/*
** exec.c for exec in /home/delafo_c/rendu/PSU_2013_minishell1/src
** 
** Made by delafo_c
** Login   <delafo_c@epitech.net>
** 
** Started on  Fri Dec 20 20:38:40 2013 delafo_c
** Last update Sun Mar  9 21:50:17 2014 delafo_c
*/

#include <stdlib.h>
#include "../include/my.h"
#include "../include/minishell2.h"

const t_builtins	g_builtins[] =
  {
    {"cd", my_cd},
    {"env", my_env},
    {"setenv", my_setenv},
    {"unsetenv", my_unsetenv},
    {NULL, NULL}
  };

/*
** Execute les builtins
** 1. Check du builtin
** 2. Pointeur sur fonction vers la fonction correspondante au builtin
*/

int		exec_builtins(t_data *data, char **cmd, char **env_tab)
{
  int		i;
  int		ret;
  int		count;

  i = 0;
  count = 0;
  while (cmd[count] != NULL)
    count++;
  while (g_builtins[i].name != NULL)
    {
      if (my_strcmp(cmd[0], g_builtins[i].name))
	{
	  ret = g_builtins[i].ptr_func(data, cmd, env_tab, count, i);
	  return (ret);
	}
      i++;
    }
  return (-1);
}
