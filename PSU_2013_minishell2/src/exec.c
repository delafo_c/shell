/*
** exec.c for exec in /home/delafo_c/rendu/PSU_2013_minishell1/src
** 
** Made by delafo_c
** Login   <delafo_c@epitech.net>
** 
** Started on  Fri Dec 20 20:38:40 2013 delafo_c
** Last update Sun Mar  9 23:24:30 2014 delafo_c
*/

#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include "../include/my.h"
#include "../include/minishell2.h"

/*
** Gère l'exécution
** 1. Passage de l'env et des cmds en tableau
** 2. Check de la commande
** 3. Execution
*/

int		gere_exec(t_cmd *cmd, t_data *data, char **environ)
{
  char		**cmd_tab;
  char		**env_tab;

  if ((cmd_tab = cmd_in_tab(cmd)) == NULL)
    return (-1);
  data->path = find_path(cmd_tab[0], environ);
  if ((env_tab = env_in_tab(data->env)) == NULL)
    return (-1);
  if (check_tok(cmd_tab, data) == BUILTINS)
    data->tok = BUILTINS;
  else
    data->tok = CMD;
  if (data->tok == BUILTINS)
    return (exec_builtins(data, cmd_tab, env_tab));
  if (data->tok == CMD)
    return (my_exec(cmd_tab, env_tab, data));
  free_tab(cmd_tab);
  return (0);
}

/*
** Check si la commande est un builtin
** Si oui, elle lui attribue le token builtin
*/

int		check_tok(char **cmd, t_data *data)
{
  if (my_strcmp(cmd[0], "env"))
    return (BUILTINS);
  else if (my_strcmp(cmd[0], "setenv"))
    return (BUILTINS);
  else if (my_strcmp(cmd[0], "unsetenv"))
    return (BUILTINS);
  else if (my_strcmp(cmd[0], "cd"))
    return (BUILTINS);
  else if (my_strcmp(cmd[0], "exit"))
    {
      free_data(data);
      exit(0);
    }
  else
    return (CMD);
}

/*
** Execution
** 1. Test de la commande
** 2. Création d'un processus fils (fork)
** 3. Execution de la commande
*/

int		my_exec(char **cmd, char **env_tab, t_data *data)
{
  pid_t		pid;

  if (access(data->path, X_OK) == -1)
    {
      my_printf_error("minish: command not found: %s\n", cmd[0]);
      return (-1);
    }
  if ((pid = fork()) == -1)
    {
      my_printf_error("minish: fork error\n");
      return (-1);
    }
  else if (pid == 0)
    {
      execve(data->path, cmd, env_tab);
      return (-1);
    }
  else
    waitpid(pid, 0, 0);
  return (0);
}

int		free_tab(char **tab)
{
  int		i;

  i = 0;
  if (tab == NULL)
    return (-1);
  while (tab[i] != NULL)
    {
      free(tab[i]);
      i++;
    }
  free(tab);
  return (0);
}
