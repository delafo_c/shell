/*
** my_str_to_dll.c for my_str_to_dll in /home/delafo_c/rendu/PSU_2013_minishell2/src
** 
** Made by delafo_c
** Login   <delafo_c@epitech.net>
** 
** Started on  Mon Feb 24 10:25:08 2014 delafo_c
** Last update Sat Mar  8 22:15:34 2014 delafo_c
*/

#include "../include/minishell2.h"

/*
** Convertion d'une chaine de caractère en liste doublement chainée
*/

t_cmd		*my_str_to_dll(char *str, t_cmd *cmd)
{
  int		i;

  i = 0;
  while (str[i])
    {
      if (str[i] == ' ' || str[i] == '\t')
	i = gere_spaces_and_tabs(str, i);
      else if (str[i] != ' ' && str[i] != '\t')
	i = gere_others(str, cmd, i);
    }
  return (cmd);
}

/*
** Ajout de la chaine de caractère dans la liste chainée
*/

int		gere_others(char *str, t_cmd *cmd, int i)
{
  add_to_list(cmd, str + i);
  while (str[i] != '\0' && str[i] != ' ' && str[i] != '\t')
    ++i;
  return (i);
}

/*
** Avance dans la chaine de caractère si il y a des espaces ou tabulations
*/

int		gere_spaces_and_tabs(char *str, int i)
{
  str[i] = '\0';
  i++;
  while (str[i] == ' ' || str[i] == '\t')
    ++i;
  return (i);
}
