/*
** env.c for env in /home/delafo_c/rendu/PSU_2013_minishell1
** 
** Made by delafo_c
** Login   <delafo_c@epitech.net>
** 
** Started on  Fri Dec 20 13:39:10 2013 delafo_c
** Last update Sun Mar  9 21:50:08 2014 delafo_c
*/

#include <stdlib.h>
#include "../include/my.h"
#include "../include/minishell2.h"

t_env		*init_dll_env(void)
{
  t_env		*env;

  if ((env = malloc(sizeof(t_env))) == NULL)
    return (NULL);
  env->next = env;
  env->prev = env;
  env->var = NULL;
  return (env);
}

t_env		*put_in_list(t_env *env, char *var)
{
  t_env		*new_elem;

  if ((new_elem = malloc(sizeof(t_env))) == NULL)
    return (NULL);
  new_elem->prev = env->prev;
  new_elem->next = env;
  env->prev->next = new_elem;
  env->prev = new_elem;
  if ((new_elem->var = my_strdup(var)) == NULL)
    return (NULL);
  return (new_elem);
}

t_env		*init_env(char **environ)
{
  int		i;
  t_env		*env;

  i = 0;
  if ((env = init_dll_env()) == NULL)
    return (NULL);
  if (environ[i] == NULL)
    return (env);
  while (environ[i] != NULL)
    if (put_in_list(env, environ[i++]) == NULL)
      return (NULL);
  return (env);
}

char		**env_in_tab(t_env *env)
{
  int		i;
  int		size;
  t_env		*user;
  char		**env_tab;

  i = 0;
  size = 0;
  user = env;
  env = env->next;
  while (env != user)
    {
      env = env->next;
      size++;
    }
  if ((env_tab = malloc(sizeof(char *) * (size + 1))) == NULL)
    return (NULL);
  env = env->next;
  while (env != user)
    {
      env_tab[i] = env->var;
      i++;
      env = env->next;
    }
  env_tab[i] = NULL;
  return (env_tab);
}
