/*
** tokenizer.c for tokenizer in /home/delafo_c/rendu/PSU_2013_minishell2/src
** 
** Made by delafo_c
** Login   <delafo_c@epitech.net>
** 
** Started on  Mon Feb 24 12:37:49 2014 delafo_c
** Last update Sun Mar  9 21:56:20 2014 delafo_c
*/

#include <stdlib.h>
#include "../include/my.h"
#include "../include/minishell2.h"

t_token         tkn_tab[] =
  {
    {SEP, TYPE_SEP},
    {PIPE, TYPE_PIPE},
    {REDI_R, TYPE_REDI_R},
    {D_REDI_R, TYPE_D_REDI_R},
    {REDI_L, TYPE_REDI_L},
    {D_REDI_L, TYPE_D_REDI_L}
  };

void		tokenizer(t_cmd *cmd)
{
  int		i;
  t_cmd		*tmp;

  tmp = cmd->next;
  while (tmp != cmd)
    {
      i = 0;
      while (tkn_tab[i].name && my_strncmp(tmp->word, tkn_tab[i].name,
					   my_strlen(tkn_tab[i].name)))
	i++;
      tmp->type = tkn_tab[i].type;
      tmp = tmp->next;
    }
}
