/*
** path.c for path in /home/delafo_c/rendu/PSU_2013_minishell1
** 
** Made by delafo_c
** Login   <delafo_c@epitech.net>
** 
** Started on  Thu Dec 19 10:02:45 2013 delafo_c
** Last update Sun Mar  9 22:06:29 2014 delafo_c
*/

#include <unistd.h>
#include <stdlib.h>
#include "../include/my.h"
#include "../include/minishell2.h"

/*
** strtok de la variable PATH
*/

char		**my_strtok_path(char *env_path)
{
  int		i;
  int		a;
  int		b;
  char		**path;

  i = 0;
  a = 0;
  if ((path = malloc(sizeof(char *) * (count_path(env_path) + 1))) == NULL)
    return (NULL);
  while (env_path[i])
    {
      b = 0;
      if ((path[a] = malloc(my_pathlen(env_path + i) + 2)) == NULL)
        return (NULL);
      while (env_path[i] && env_path[i] != ':')
        path[a][b++] = env_path[i++];
      path[a][b++] = '/';
      path[a++][b] = '\0';
      if (env_path[i])
        i++;
    }
  path[a] = NULL;
  return (path);
}

/*
** Récupère la variable PATH de l'environnement
*/

char		*get_path(char **env)
{
  int		i;

  i = 0;
  while (env[i] && my_strncmp(env[i], "PATH=", 5) == 0)
    i++;
  if (env[i] == NULL)
    return (NULL);
  else
    return (env[i] + 5);
}

/*
** Test le path avec la fonction access
*/

char		*try_path(char *cmd, char **path)
{
  int		i;
  char		*good_path;

  i = 0;
  while (path && path[i])
    {
      if ((good_path = my_strcat(path[i], cmd)) == NULL)
	return (NULL);
      if (access(good_path, X_OK) == 0)
	return (good_path);
      i++;
    }
  return (my_strdup(cmd));
}

/*
** Compte le nombre de path pour malloc le tableau
*/

int		count_path(char *path)
{
  int		i;
  int		nb;

  i = 0;
  nb = 1;
  while (path[i])
    {
      if (path[i] == ':')
        nb++;
      i++;
    }
  return (nb);
}

/*
** Compte le nombre de caractère d'un path
*/

int		my_pathlen(char *path)
{
  int		i;

  i = 0;
  while (path[i] && path[i] != ':')
    i++;
  return (i);
}
