/*
** data.c for data in /home/delafo_c/rendu/PSU_2013_minishell1
** 
** Made by delafo_c
** Login   <delafo_c@epitech.net>
** 
** Started on  Fri Dec 20 14:07:21 2013 delafo_c
** Last update Sat Mar  8 22:06:56 2014 delafo_c
*/

#include <stdlib.h>
#include "../include/minishell2.h"

/*
** Initialisation de la structure principale data
*/

t_data		*init_data(char **environ)
{
  t_data	*data;

  if ((data = malloc(sizeof(t_data))) == NULL)
    return (NULL);
  if ((data->env = init_env(environ)) == NULL)
    return (NULL);
  data->path = NULL;
  data->home = NULL;
  data->pwd = NULL;
  data->old_pwd = NULL;
  return (data);
}

/*
** Récupère le path
** 1. Récupère le path de l'environnement
** 2. Transforme le path en tableau afin d'effectuer des tests
** 3. Test le path pour executer la commande
*/

char		*find_path(char *cmd, char **env)
{
  int		i;
  char		**path;
  char		*env_path;
  char		*good_path;

  i = 0;
  if ((env_path = get_path(env)) == NULL)
    return (NULL);
  if ((path = my_strtok_path(env_path)) == NULL)
    return (NULL);
  if ((good_path = try_path(cmd, path)) == NULL)
    return (NULL);
  while (path[i])
    free(path[i++]);
  free(path);
  return (good_path);
}

/*
** Récupère la variable HOME de l'environnement
*/

char		*get_home(char **env)
{
  int		i;

  i = 0;
  while (env[i] && my_strncmp(env[i], "HOME=", 5) == 0)
    i++;
  if (env[i] == NULL)
    return (NULL);
  else
    return (env[i] + 5);
}

/*
** Récupère la variable PWD de l'environnement
*/

char		*get_pwd(char **env)
{
  int		i;

  i = 0;
  while (env[i] && my_strncmp(env[i], "PWD=", 4) == 0)
    i++;
  if (env[i] == NULL)
    return (NULL);
  else
    return (env[i] + 4);
}

/*
** Libère les données de la structure data
*/

void		free_data(t_data *data)
{
  int		i;
  t_env		*tmp;

  i = 0;
  tmp = data->env->next;
  while (tmp != data->env)
    {
      tmp = tmp->next;
      free(tmp->prev->var);
      free(tmp->prev);
    }
  free(tmp);
  free(data->path);
  free(data);
}
