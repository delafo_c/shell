/*
** cmd.c for cmd in /home/delafo_c/rendu/PSU_2013_minishell2/src
** 
** Made by delafo_c
** Login   <delafo_c@epitech.net>
** 
** Started on  Mon Feb 24 10:15:38 2014 delafo_c
** Last update Sun Mar  9 21:49:53 2014 delafo_c
*/

#include <stdlib.h>
#include "../include/my.h"
#include "../include/minishell2.h"

/*
** Initialisation de la double liste chainée pour stocker les commandes
*/

t_cmd		*init_cmd(char *command)
{
  t_cmd		*cmd;

  if ((cmd = init_dll_cmd()) == NULL || my_str_to_dll(command, cmd) == NULL)
    return (NULL);
  return (cmd);
}

t_cmd		*init_dll_cmd(void)
{
  t_cmd		*cmd;

  if ((cmd = malloc(sizeof(t_cmd))) == NULL)
    return (NULL);
  cmd->next = cmd;
  cmd->prev = cmd;
  return (cmd);
}

/*
** Ajout des mots dans la liste chainée
*/

t_cmd		*add_to_list(t_cmd *cmd, char *word)
{
  t_cmd		*new_elem;

  if ((new_elem = malloc(sizeof(t_cmd))) == NULL)
    return (NULL);
  new_elem->type = 0;
  new_elem->word = word;
  new_elem->next = cmd;
  new_elem->prev = cmd->prev;
  cmd->prev->next = new_elem;
  cmd->prev = new_elem;
  return (new_elem);
}

/*
** Conversion de la liste chainée des commandes en tableau
*/

char		**cmd_in_tab(t_cmd *cmd)
{
  int		i;
  t_cmd		*user;
  char		**cmd_tab;

  i = 0;
  user = cmd;
  cmd = cmd->next;
  while (cmd != user)
    {
      cmd = cmd->next;
      i++;
    }
  if ((cmd_tab = malloc(sizeof(char *) * (i + 1))) == NULL)
    return (NULL);
  i = 0;
  cmd = cmd->next;
  while (cmd != user)
    {
      cmd_tab[i] = cmd->word;
      i++;
      cmd = cmd->next;
    }
  cmd_tab[i] = NULL;
  return (cmd_tab);
}

void		free_command(char *cmd, t_cmd *command)
{
  t_cmd		*tmp;

  tmp = command->next;
  while (tmp != command)
    {
      tmp = tmp->next;
      free(tmp->prev);
    }
  free(command);
  free(cmd);
}
