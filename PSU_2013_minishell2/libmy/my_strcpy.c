/*
** my_strcpy.c for my_strcpy in /home/delafo_c/rendu/Piscine-C-Jour_06/ex_01
** 
** Made by delafo_c
** Login   <delafo_c@epitech.net>
** 
** Started on  Mon Oct  7 09:12:29 2013 delafo_c
** Last update Fri Nov 29 18:23:00 2013 delafo_c
*/

char	*my_strcpy(char *dest, char *src)
{
  int	i;

  i = 0;
  while (src[i])
    {
      dest[i] = src[i];
      i++;
    }
  return (dest);
}
