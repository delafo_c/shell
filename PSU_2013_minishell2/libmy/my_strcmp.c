/*
** my_strcmp.c for my_strcmp in /home/delafo_c/rendu/Piscine-C-lib/my
** 
** Made by delafo_c
** Login   <delafo_c@epitech.net>
** 
** Started on  Wed Oct  9 17:32:11 2013 delafo_c
** Last update Thu Dec 19 12:14:27 2013 delafo_c
*/

#include "my.h"

int		my_strcmp(char *s1, char *s2)
{
  int		i;

  i = 0;
  while (s1[i] && s2[i] && s1[i] == s2[i])
    {
      if (s1[i] != s2[i])
	return (0);
      i++;
    }
  if (!s1[i] && !s2[i])
    return (i);
  return (0);
}
