/*
** printf_flag3.c for printf_flag in /home/delafo_c/travail/PrintF/flags
** 
** Made by delafo_c
** Login   <delafo_c@epitech.net>
** 
** Started on  Fri Nov 15 19:37:10 2013 delafo_c
** Last update Thu Dec 12 14:20:07 2013 delafo_c
*/

#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include "../../include/my_printf.h"

int		printf_percent(va_list ap)
{
  my_putchar('%');
  return (1);
}
