/*
** printf_flag2.c for printf_flag2.c in /home/delafo_c/travail/PrintF/flags
** 
** Made by delafo_c
** Login   <delafo_c@epitech.net>
** 
** Started on  Fri Nov 15 19:34:48 2013 delafo_c
** Last update Thu Dec 12 14:19:37 2013 delafo_c
*/

#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include "../../include/my_printf.h"

int		printf_bin(va_list ap)
{
  int		result;
  int		nb;

  nb = 0;
  result = 0;
  nb = va_arg(ap, unsigned int);
  result = my_putnbr_base_printf(nb, "01", 0);
  return (result);
}

int		printf_c(va_list ap)
{
  int		result;
  int		nb;

  nb = 0;
  result = 0;
  nb = va_arg(ap, int);
  if (nb >= 0 && nb <= 127)
    {
      my_putchar(nb);
      result++;
    }
  return (result);
}

int		printf_s(va_list ap)
{
  int		result;
  char		*str;
  int		i;

  i = 0;
  result = 0;
  str = va_arg(ap, char *);
  if (str == NULL)
    {
      my_putstr("(null)");
      result += 6;
    }
  while (str && str[i])
    {
      my_putchar(str[i]);
      i++;
      result++;
    }
  return (result);
}

int		printf_smaj(va_list ap)
{
  int		result;
  char		*str;
  int		i;
  int		c;

  i = 0;
  result = 0;
  str = va_arg(ap, char *);
  while (str && str[i])
    {
      c = (int) str[i];
      if (c > 32 && c <= 127)
        my_putchar(str[i]);
      else
        {
          my_putstr("\\");
          my_putchar('0');
          result += 2 + my_putnbr_base_printf(str[i], "01234567", 0);
        }
      i++;
      result++;
    }
  return (result);
}

int		printf_p(va_list ap)
{
  int		result;
  int		nb;

  nb = 0;
  my_putstr("0x");
  result = 2;
  nb = va_arg(ap, unsigned int);
  result += my_putnbr_base_printf(nb, "0123456789abcdef", result);
  return (result);
}
