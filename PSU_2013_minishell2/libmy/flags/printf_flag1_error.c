/*
** printf_flag1.c for printf_flag in /home/delafo_c/travail/PrintF/flags
** 
** Made by delafo_c
** Login   <delafo_c@epitech.net>
** 
** Started on  Fri Nov 15 19:30:31 2013 delafo_c
** Last update Sat Jan  4 17:15:32 2014 delafo_c
*/

#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include "../../include/my_printf.h"

int			printf_dec_error(va_list ap)
{
  int			result;
  unsigned int		nb;

  nb = 0;
  result = 0;
  nb = va_arg(ap, int);
  result = my_put_nbr_printf_error(nb);
  return (result);
}

int			printf_octal_error(va_list ap)
{
  int			result;
  int			nb;

  nb = 0;
  result = 0;
  nb = va_arg(ap, unsigned int);
  result = my_putnbr_base_printf_error(nb, "01234567", 0);
  return (result);
}

int			printf_hex_error(va_list ap)
{
  int			result;
  int			nb;

  nb = 0;
  result = 0;
  nb = va_arg(ap, unsigned int);
  result = my_putnbr_base_printf_error(nb, "0123456789abcdef", 0);
  return (result);
}

int			printf_hexmaj_error(va_list ap)
{
  int			result;
  int			nb;

  nb = 0;
  result = 0;
  nb = va_arg(ap, unsigned int);
  result = my_putnbr_base_printf_error(nb, "0123456789ABCDEF", 0);
  return (result);
}

int			printf_u_error(va_list ap)
{
  int			result;
  int			nb;

  nb = 0;
  result = 0;
  nb = va_arg(ap, unsigned int);
  result = my_putnbr_base_printf_error(nb, "0123456789", 0);
  return (result);
}
