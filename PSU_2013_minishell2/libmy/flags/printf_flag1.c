/*
** printf_flag1.c for printf_flag in /home/delafo_c/travail/PrintF/flags
** 
** Made by delafo_c
** Login   <delafo_c@epitech.net>
** 
** Started on  Fri Nov 15 19:30:31 2013 delafo_c
** Last update Thu Dec 12 14:18:59 2013 delafo_c
*/

#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include "../../include/my_printf.h"

int			printf_dec(va_list ap)
{
  int			result;
  unsigned int		nb;

  nb = 0;
  result = 0;
  nb = va_arg(ap, int);
  result = my_put_nbr_printf(nb);
  return (result);
}

int			printf_octal(va_list ap)
{
  int			result;
  int			nb;

  nb = 0;
  result = 0;
  nb = va_arg(ap, unsigned int);
  result = my_putnbr_base_printf(nb, "01234567", 0);
  return (result);
}

int			printf_hex(va_list ap)
{
  int			result;
  int			nb;

  nb = 0;
  result = 0;
  nb = va_arg(ap, unsigned int);
  result = my_putnbr_base_printf(nb, "0123456789abcdef", 0);
  return (result);
}

int			printf_hexmaj(va_list ap)
{
  int			result;
  int			nb;

  nb = 0;
  result = 0;
  nb = va_arg(ap, unsigned int);
  result = my_putnbr_base_printf(nb, "0123456789ABCDEF", 0);
  return (result);
}

int			printf_u(va_list ap)
{
  int			result;
  int			nb;

  nb = 0;
  result = 0;
  nb = va_arg(ap, unsigned int);
  result = my_putnbr_base_printf(nb, "0123456789", 0);
  return (result);
}
