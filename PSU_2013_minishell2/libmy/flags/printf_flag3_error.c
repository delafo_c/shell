/*
** printf_flag3.c for printf_flag in /home/delafo_c/travail/PrintF/flags
** 
** Made by delafo_c
** Login   <delafo_c@epitech.net>
** 
** Started on  Fri Nov 15 19:37:10 2013 delafo_c
** Last update Sat Jan  4 17:17:48 2014 delafo_c
*/

#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include "../../include/my_printf.h"

int		printf_percent_error(va_list ap)
{
  my_putchar_error('%');
  return (1);
}
