/*
** my_put_nbr.c for my_put_nbr in /home/delafo_c/rendu/Piscine-C-Jour_03
** 
** Made by delafo_c
** Login   <delafo_c@epitech.net>
** 
** Started on  Wed Oct  2 16:07:31 2013 delafo_c
** Last update Fri Dec  6 16:47:24 2013 delafo_c
*/

#include "my.h"

int     my_put_nbr(int nb)
{
  if (nb < 0)
    {
      my_putchar('-');
      nb = -nb;
    }
  if (nb / 10 != 0)
    my_put_nbr(nb / 10);
  my_putchar((nb % 10) + '0');
}
