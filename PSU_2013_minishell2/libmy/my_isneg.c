/*
** my_isneg.c for my_isneg in /home/delafo_c/rendu/Piscine-C-Jour_03
** 
** Made by delafo_c
** Login   <delafo_c@epitech.net>
** 
** Started on  Wed Oct  2 12:50:36 2013 delafo_c
** Last update Fri Dec  6 16:42:00 2013 delafo_c
*/

void	my_isneg(int n)
{
  if (n >= 0)
    my_putchar(80);
  else
    my_putchar(78);
}
