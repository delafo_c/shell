/*
** my_power_rec.c for my_power_rec in /home/delafo_c/rendu/Piscine-C-Jour_05
** 
** Made by delafo_c
** Login   <delafo_c@epitech.net>
** 
** Started on  Sun Oct  6 16:33:53 2013 delafo_c
** Last update Tue Oct 29 15:15:10 2013 delafo_c
*/

int	my_power_rec(int nb, int power)
{
  if (power == 0)
    return (1);
  if (power < 0 || nb >= 1073741824)
    return (0);
  if (power > 1)
    {
      nb = nb * my_power_rec(nb, power - 1);
      return (nb);
    }
}
