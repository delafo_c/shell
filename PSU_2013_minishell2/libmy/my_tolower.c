/*
** my_tolower.c for my_tolower in /home/delafo_c/rendu/PSU_2013_my_ls/libmy
** 
** Made by delafo_c
** Login   <delafo_c@epitech.net>
** 
** Started on  Fri Nov 29 18:16:29 2013 delafo_c
** Last update Fri Dec  6 16:56:06 2013 delafo_c
*/

#include "my.h"
#include <stdio.h>

char	*my_tolower(char *str)
{
  int	i;

  i = 0;
  if (str == NULL)
    return (NULL);
  while (str[i])
    {
      if (str[i] > 64 && str[i] < 91)
	str[i] += 32;
      i++;
    }
  return (str);
}
