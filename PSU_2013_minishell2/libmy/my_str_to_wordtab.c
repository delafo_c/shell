/*
** my_str_to_wordtab.c for my_str_to_wordtab in /home/delafo_c/rendu/Piscine-C-Jour_08/ex_04
** 
** Made by delafo_c
** Login   <delafo_c@epitech.net>
** 
** Started on  Fri Oct 11 09:31:48 2013 delafo_c
** Last update Wed Dec 11 21:49:24 2013 delafo_c
*/

#include <stdlib.h>
#include <stdio.h>

int		my_countword(char *str)
{
  int		i;
  int		count;

  i = 0;
  count = 2;
  while (str[i] != '\0' && str[i] != '\n')
    {
      if (str[i] == ' ')
	count++;
      i++;
    }
  return (count);
}

int		my_countchar(char *str)
{
  int	count;

  count = 0;
  while (str[count] != ' ' && str[count] != '\0' && str[count] != '\n')
    count++;
  count++;
  return (count);
}

char		**my_str_to_wordtab(char *str)
{
  char		**tab;
  int		i;
  int		a;
  int		b;

  i = 0;
  a = 0;
  b = 0;
  if (str[i] == ' ')
    while (str[i] == ' ')
      i++;
  if ((tab = malloc(sizeof(*tab) * (my_countword(str)))) == NULL)
    return (NULL);
  while (str[i] != '\n' && str[i] != '\0')
    {
      if (str[i] == ' ' || str[i] == '\n')
	{
	  while (str[i] == ' ')
	    i++;
	  a++;
	  b = 0;
	}
      if ((tab[a] = malloc(sizeof(**tab) * (my_countchar(str + i)))) == NULL)
	return (NULL);
      while ((str[i] != ' ')  && (str[i] != '\n') && (str[i] != '\0'))
	tab[a][b++] = str[i++];
      tab[a][b] = '\0';
    }
  tab[a + 1] = 0;
  return (tab);
}

void		my_show_tab(char **tab)
{
  int		i;

  i = 0;
  while (tab[i] != NULL)
    {
      printf("%s\n", tab[i]);
      i++;
    }
  return ;
}
