/*
** my_strlcat.c for my_strlcat in /home/delafo_c/rendu/Piscine-C-Jour_07/ex_01
** 
** Made by delafo_c
** Login   <delafo_c@epitech.net>
** 
** Started on  Tue Oct  8 18:09:28 2013 delafo_c
** Last update Tue Oct 29 15:10:14 2013 delafo_c
*/

int	my_strlcat(char *dest, char *src, int size)
{
  int	dest_lenght;
  int	src_lenght;
  int	a;
  int	b;

  a = 0;
  b = 0;
  dest_lenght = my_strlen(dest);
  src_lenght = my_strlen(src);
  while (a < size)
    {
      if (a >= dest_lenght && src[b])
	{
	  dest[a] = src[b];
	  b++;
	}
      a++;
    }
  a--;
  dest[a] = '\0';
  if (size >= dest_lenght || size == 0)
    return (dest_lenght + src_lenght);
  if (size < dest_lenght)
    return (src_lenght + size);
}
