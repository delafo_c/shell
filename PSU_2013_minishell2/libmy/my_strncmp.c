/*
** my_strncmp.c for my_strncmp in /home/delafo_c/rendu/Piscine-C-lib/my
** 
** Made by delafo_c
** Login   <delafo_c@epitech.net>
** 
** Started on  Wed Oct  9 19:27:10 2013 delafo_c
** Last update Wed Dec 18 18:20:02 2013 delafo_c
*/

int		my_strncmp(char *s1, char *s2, int nb)
{
  int		i;

  i = 0;
  while (s1[i] && s2[i] && i < nb)
    {
      if (s1[i] != s2[i])
	return (0);
      i++;
    }
  if (i == nb)
    return (1);
  return (0);
}
