/*
** my_strlen.c for my_strlen in /home/delafo_c/rendu/Piscine-C-Jour_04
** 
** Made by delafo_c
** Login   <delafo_c@epitech.net>
** 
** Started on  Fri Oct  4 12:20:51 2013 delafo_c
** Last update Tue Oct 29 15:08:58 2013 delafo_c
*/

int	my_strlen(char *str)
{
  int i;

  i = 0;
  while (*str)
    {
      i++;
      str++;
    }
  return (i);
}
