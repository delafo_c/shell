/*
** get_next_line.c for get_next_line in /home/delafo_c/travail/GetNextLine
** 
** Made by delafo_c
** Login   <delafo_c@epitech.net>
** 
** Started on  Mon Nov 18 21:03:33 2013 delafo_c
** Last update Sun Mar  9 22:24:59 2014 delafo_c
*/

#include <stdlib.h>
#include <unistd.h>
#include "../include/get_next_line.h"

char		*get_next_line(const int fd)
{
  int		cur;
  int	        check;
  char		*next_line;
  static int	save = 0;
  static char	buf[BUFSIZE + 1];

  cur = 0;
  check = 0;
  next_line = NULL;
  if ((check = read(fd, buf + save, NBCHAR - save)) <= 0)
    return (NULL);
  if (buf[cur])
    {
      if (buf[cur] == '\n')
	return (next_line);
      if ((next_line = malloc(sizeof(next_line) * BUFSIZE + 1)) == NULL)
	return (NULL);
      empty(next_line);
      while (buf[cur] && buf[cur] != '\n')
	next_line[cur] = buf[cur++];
      if (buf[cur] == '\n')
	cur++;
      save = relocbuf(buf, cur);
    }
  return (next_line);
}

int	        relocbuf(char *buf, int cur)
{
  static int    pos;
  int		end;

  pos = 0;
  while (buf[cur])
    {
      buf[pos++] = buf[cur++];
    }
  buf[pos] = '\0';
  end = pos;
  while (buf[++end])
    {
      buf[end] = '\0';
    }
  return (pos);
}

void		empty(char *next_line)
{
  int		i;

  i = 0;
  while (i < (BUFSIZE + 1))
    next_line[i++] = 0;
}
