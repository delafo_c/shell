/*
** my_getnbr_base.c for my_getnbr_base in /home/delafo_c/travail/Lib/my
** 
** Made by delafo_c
** Login   <delafo_c@epitech.net>
** 
** Started on  Tue Nov 12 23:37:13 2013 delafo_c
** Last update Thu Nov 14 16:19:01 2013 delafo_c
*/

int	counter(char c, char *str)
{
  int	i;

  i = 0;
  while (str[i])
    {
      if (str[i] == c)
	return (i);
      i++;
    }
  return (-1);
}

int	my_getnbr_base(char *str, char *base)
{
  int	result;
  int	len_base;
  int	len_str;
  int	fact;

  fact = 1;
  result = 0;
  len_base = my_strlen(base);
  len_str = my_strlen(str) - 1;
  if (str[0] == '-')
    return (-my_getnbr_base(&str[1], base));
  while (len_str >= 0)
    {
      result = result + find_pos(str[len_str], base) * fact;
      fact = len_base * fact;
      len_str = len_str - 1;
    }
  return (result);
}
