/*
** my_putnbr_base.c for my_putnbr_base in /home/delafo_c/travail/Lib/my
** 
** Made by delafo_c
** Login   <delafo_c@epitech.net>
** 
** Started on  Tue Nov 12 23:51:34 2013 delafo_c
** Last update Thu Nov 14 16:15:26 2013 delafo_c
*/

#include "my.h"

int	my_putnbr_base(int nb, char *base)
{
  if (nb < 0)
    {
      my_putchar('-');
      nb = -nb;
    }
  if (nb >= my_strlen(base) - 1)
    my_putnbr_base(nb / my_strlen(base), base);
  my_putchar(base[nb % my_strlen(base)]);
  return (nb);
}
