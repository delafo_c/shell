/*
** my_put_printf.c for my_put_printf in /home/delafo_c/travail/PrintF
** 
** Made by delafo_c
** Login   <delafo_c@epitech.net>
** 
** Started on  Fri Nov 15 19:53:21 2013 delafo_c
** Last update Sat Jan  4 17:11:08 2014 delafo_c
*/

#include "../include/my_printf.h"

int	my_put_nbr_printf(int nb)
{
  int	result;

  result = 0;
  if (nb == -2147483648)
    {
      my_putstr("-2147483648");
      result += 11;
    }
  else
    {
      if (nb < 0)
        {
          my_putchar('-');
          nb = -nb;
          result++;
        }
      if (nb / 10 != 0)
        result += my_put_nbr_printf(nb / 10);
      result++;
      my_putchar((nb % 10) + '0');
    }
  return (result);
}

int	my_put_nbr_printf_error(int nb)
{
  int	result;

  result = 0;
  if (nb == -2147483648)
    {
      my_putstr_error("-2147483648");
      result += 11;
    }
  else
    {
      if (nb < 0)
	{
	  my_putchar_error('-');
	  nb = -nb;
	  result++;
	}
      if (nb / 10 != 0)
	result += my_put_nbr_printf_error(nb / 10);
      result++;
      my_putchar_error((nb % 10) + '0');
    }
  return (result);
}

int	my_putnbr_base_printf(unsigned int nb, char *base, int result)
{
  int	i;
  int   str_len;

  str_len = my_strlen(base);
  i = nb % str_len;
  nb = nb / str_len;
  if (nb > 0)
    result += my_putnbr_base_printf(nb, base, 0);
  my_putchar(base[i]);
  result++;
  return (result);
}

int	my_putnbr_base_printf_error(unsigned int nb, char *base, int result)
{
  int	i;
  int	str_len;

  str_len = my_strlen(base);
  i = nb % str_len;
  nb = nb / str_len;
  if (nb > 0)
    result += my_putnbr_base_printf_error(nb, base, 0);
  my_putchar_error(base[i]);
  result++;
  return (result);
}
