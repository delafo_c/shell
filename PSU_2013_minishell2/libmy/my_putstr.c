/*
** my_putstr.c for my_putstr in /home/delafo_c/rendu/Piscine-C-Jour_04
** 
** Made by delafo_c
** Login   <delafo_c@epitech.net>
** 
** Started on  Thu Oct  3 18:36:39 2013 delafo_c
** Last update Sat Jan  4 17:12:05 2014 delafo_c
*/

void	my_putstr(char *str)
{
  while (*str)
    {
      my_putchar(*str);
      str++;
    }
}

void	my_putstr_error(char *str)
{
  while (*str)
    {
      my_putchar_error(*str);
      str++;
    }
}
