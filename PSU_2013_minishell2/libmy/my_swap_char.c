/*
** my_swap_char.c for my_swap_char.c in /home/delafo_c/rendu/PSU_2013_minishell1/libmy
** 
** Made by delafo_c
** Login   <delafo_c@epitech.net>
** 
** Started on  Mon Jan  6 19:19:36 2014 delafo_c
** Last update Tue Jan  7 15:19:38 2014 delafo_c
*/

void	my_swap_char(char **a, char **b)
{
  char	*x;

  x = *a;
  *a = *b;
  *b = x;
}
