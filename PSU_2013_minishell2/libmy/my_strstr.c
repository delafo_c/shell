/*
** my_strstr.c for my_strstr in /home/delafo_c/rendu/Piscine-C-Jour_06/ex_04
** 
** Made by delafo_c
** Login   <delafo_c@epitech.net>
** 
** Started on  Mon Oct  7 10:16:42 2013 delafo_c
** Last update Fri Dec  6 16:53:32 2013 delafo_c
*/

#include <stdio.h>
#include "my.h"

char	*my_strstr(char *str, char *to_find)
{
  int	i;
  int	j;

  i = 0;
  j = 0;
  while (str[i])
    {
      if (str[i] == to_find[j])
	  j++;
      i++;
    }
  if (j == my_strlen(to_find))
      return (&str[i]);
  else
    return (NULL);
}
