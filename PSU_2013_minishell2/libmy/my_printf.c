/*
** my_printf.c for my_printf in /home/delafo_c/travail/PrintF
** 
** Made by delafo_c
** Login   <delafo_c@epitech.net>
** 
** Started on  Fri Nov 15 16:48:01 2013 delafo_c
** Last update Mon Jan  6 18:19:18 2014 delafo_c
*/

#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include "../include/my_printf.h"
#include "../include/my.h"

char		*create_printf_tab()
{
  char		*printf_tab;

  if ((printf_tab = malloc(sizeof(printf_tab) * 13)) == NULL)
    return (NULL);
  printf_tab[0] = 'd';
  printf_tab[1] = 'i';
  printf_tab[2] = 'o';
  printf_tab[3] = 'x';
  printf_tab[4] = 'X';
  printf_tab[5] = 'u';
  printf_tab[6] = 'b';
  printf_tab[7] = 'c';
  printf_tab[8] = 's';
  printf_tab[9] = 'S';
  printf_tab[10] = 'p';
  printf_tab[11] = '%';
  printf_tab[12] = '\0';
  return (printf_tab);
}

handler		*create_printf_handler()
{
  handler	*printf_flag;

  if ((printf_flag = malloc(sizeof(printf_flag) * 13)) == NULL)
    return (NULL);
  printf_flag[0] = &printf_dec;
  printf_flag[1] = &printf_dec;
  printf_flag[2] = &printf_octal;
  printf_flag[3] = &printf_hex;
  printf_flag[4] = &printf_hexmaj;
  printf_flag[5] = &printf_u;
  printf_flag[6] = &printf_bin;
  printf_flag[7] = &printf_c;
  printf_flag[8] = &printf_s;
  printf_flag[9] = &printf_smaj;
  printf_flag[10] = &printf_p;
  printf_flag[11] = &printf_percent;
  printf_flag[12] = &printf_s;
  return (printf_flag);
}

int		my_printf(const char *format, ...)
{
  va_list	ap;
  int		result;
  char		*printf_tab;
  handler	*printf_flag;

  va_start(ap, format);
  printf_tab = create_printf_tab();
  printf_flag = create_printf_handler();
  result = my_printf_norme(format, ap, printf_tab, printf_flag);
  va_end(ap);
  return (result);
}

int		my_printf_norme(const char *format, va_list ap,
				char *printf_tab, handler *printf_flag)
{
  int		i;
  int		check;
  int		result;

  i = 0;
  result = 0;
  if (format == NULL)
    return (-1);
  while (format && format[i])
    {
      if (format[i] == '%')
        check = check_flag(format[i + 1], printf_tab);
      if (format[i] == '%' && check < 12)
	{
	  result = result + printf_flag[check](ap) - 1;
	  i++;
	}
      else
	my_putchar(format[i]);
      result++;
      i++;
    }
  free(printf_tab);
  free(printf_flag);
  return (result);
}

int		check_flag(char format, char *str)
{
  int		printf_tab;

  printf_tab = 0;
  while (str && str[printf_tab] && format != str[printf_tab])
    printf_tab++;
  return (printf_tab);
}
