/*
** my_getnbr.c for my_getnbr in /home/delafo_c/rendu/Piscine-C-lib/my
** 
** Made by delafo_c
** Login   <delafo_c@epitech.net>
** 
** Started on  Wed Oct  9 17:23:19 2013 delafo_c
** Last update Tue Oct 29 15:16:09 2013 delafo_c
*/

int	my_getnbr(char *str)
{
  int	result;
  int	puiss;

  result = 0;
  puiss = 1;
  while (((*str) == '-') || ((*str) == '+'))
    {
      if (*str == '-')
        puiss = puiss * -1;
      str++;
    }
  while ((*str >= '0') && (*str <= '9'))
    {
      result = (result * 10) + ((*str) - '0');
      str++;
    }
  return (result * puiss);
}
