/*
** my_strncat.c for my_strncat in /home/delafo_c/rendu/Piscine-C-Jour_07/ex_02
** 
** Made by delafo_c
** Login   <delafo_c@epitech.net>
** 
** Started on  Tue Oct  8 11:39:30 2013 delafo_c
** Last update Fri Dec  6 16:52:38 2013 delafo_c
*/

char	*my_strncat(char *dest, char *src, int n)
{
  int	dest_len;
  int	i;

  i = 0;
  dest_len = my_strlen(dest);
  while (i < n && src[i])
    {
      dest[dest_len + i] = src[i];
      i++;
    }
  dest[dest_len + i] = '\0';
  return (dest);
}
