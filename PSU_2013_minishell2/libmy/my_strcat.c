/*
** my_strcat.c for my_strcat in /home/delafo_c/rendu/Piscine-C-Jour_07/ex_01
** 
** Made by delafo_c
** Login   <delafo_c@epitech.net>
** 
** Started on  Tue Oct  8 10:57:20 2013 delafo_c
** Last update Mon Feb 24 19:23:27 2014 delafo_c
*/

#include <stdlib.h>
#include "my.h"

char		*my_strcat(char *dest, char *src)
{
  int		i;
  int		j;
  char		*str;

  i = 0;
  j = 0;
  if ((str = malloc((my_strlen(dest)) + (my_strlen(src)) + 1)) == NULL)
    return (NULL);
  while (dest && dest[i])
    {
      str[i] = dest[i];
      i++;
    }
  while (src[j])
    {
      str[i] = src[j];
      i++;
      j++;
    }
  str[i] = '\0';
  return (str);
}
