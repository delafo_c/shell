/*
** my_revstr.c for my_revstr in /home/delafo_c/rendu/Piscine-C-Jour_06/ex_03
** 
** Made by delafo_c
** Login   <delafo_c@epitech.net>
** 
** Started on  Mon Oct  7 10:13:11 2013 delafo_c
** Last update Tue Oct 29 15:11:35 2013 delafo_c
*/

char	*my_revstr(char *str)
{
  int	a;
  int	b;
  char	swap;

  a = my_strlen(str);
  b = 0;
  a--;
  while (b < a / 2)
    {
      swap = str[b];
      str[b] = str[a];
      str[a] = swap;
      a--;
      b++;
    }
  return (str);
}
