/*
** my_putchar.c for my_putchar in /home/delafo_c/rendu/Piscine-C-lib/my
** 
** Made by delafo_c
** Login   <delafo_c@epitech.net>
** 
** Started on  Tue Oct  8 10:49:22 2013 delafo_c
** Last update Thu Jan 16 17:43:58 2014 delafo_c
*/

void	my_putchar(char c)
{
  write(1, &c, 1);
}

void	my_putchar_error(char c)
{
  write(2, &c, 1);
}
