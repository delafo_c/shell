/*
** my_strncpy.c for my_strncpy in /home/delafo_c/rendu/Piscine-C-Jour_06/ex_02
** 
** Made by delafo_c
** Login   <delafo_c@epitech.net>
** 
** Started on  Mon Oct  7 09:42:05 2013 delafo_c
** Last update Tue Oct 29 15:05:45 2013 delafo_c
*/

char	*my_strncpy(char *dest, char *src, int n)
{
  int	i;

  i = 0;
  while (i < n && src[i])
    {
      dest[i] = src[i];
      i++;
    }
  while (i < n)
    {
      dest[i] = '\0';
      i++;
    }
  return (dest);
}
