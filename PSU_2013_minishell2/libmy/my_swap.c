/*
** my_swap.c for my_swap in /home/delafo_c/rendu
** 
** Made by delafo_c
** Login   <delafo_c@epitech.net>
** 
** Started on  Thu Oct  3 09:03:22 2013 delafo_c
** Last update Fri Dec  6 16:54:12 2013 delafo_c
*/

void	my_swap(int *a, int *b)
{
  int	x;

  x = *a;
  *a = *b;
  *b = x;
}
