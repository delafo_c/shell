/*
** my_charis.c for my_charis in /home/delafo_c/rendu/Piscine-C-lib/my
** 
** Made by delafo_c
** Login   <delafo_c@epitech.net>
** 
** Started on  Tue Oct 29 15:54:41 2013 delafo_c
** Last update Thu Dec 12 14:22:04 2013 delafo_c
*/

#include "../include/my.h"

int	is_op(char c)
{
  if (c == '+' || c == '-' ||
      c == '*' || c == '/' ||
      c == '%')
    return (1);
  else
    return (0);
}

int	is_num(char c)
{
  if (c >= '0' && c <= '9')
    return (1);
  else
    return (0);
}

int	is_alpha(char c)
{
  return (c > 47 && c < 58 || (c > 64 && c < 91) || (c > 96 && c < 123));
}
